<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<body>
<form action="http://localhost:9999/filter/cruise" method="post">
    <table style="text-align: left">
        <tr>
            <td>Start date:</td>
            <td><input type="text" name="start date" align="left"/></td>
        </tr>
        <tr>
            <td>Finish date:</td>
            <td><input type="text" name="finish date" align="left"/></td>
        </tr>
        <tr>
            <td>Ship num:</td>
            <td><select name="ship num">
                <c:forEach items="${ships}" var="ship">
                    <option value="${ship.id}">${ship.id}</option>
                </c:forEach>
            </select></td>
        </tr>
        <tr>
            <td>Route:</td>
            <td><input type="text" name="route" align="left"/></td>
        </tr>
        <tr>
            <td>Picture:</td>
            <td><input type="text" name="picture" align="left"/></td>
        </tr>
    </table>
    <input type="hidden" name="Lang" value="${Lang}">
    <input type="submit" value="Add New Cruise"/>
</form>
</body>
</html>