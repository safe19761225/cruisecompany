<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title>Select route - CodeJava.net</title>
</head>
<body>

<div align="center">
    <h2>Select route</h2>

    <form action="http://localhost:9999/filter/order" method="post">
        <table border="1" cellpadding="5">
            <tr id="cruiseRoute">
                <th>ID</th>
                <th>Route</th>
                <th>Start Date</th>
                <th>Ship</th>
            </tr>
            <c:forEach var="cruise" items="${cruiseList}">
                <tr>
                    <td><c:out value="${cruise.getId()}"/></td>
                    <td><c:out value="${cruise.getRoute()}"/></td>
                    <td><c:out value="${cruise.getStartDate()}"/></td>
                    <td><c:out value="${cruise.getShipId()}"/></td>
                    <td><img src="<c:url value="${cruise.getPicture()}"/>"></td>
                </tr>
            </c:forEach>
        </table>
    </form>
    <br/><br/>
    <form action="http://localhost:9999/filter/order" method="post">
        Select a Route:&nbsp;
        <select name="cruiseRoute">
            <c:forEach items="${cruiseList}" var="cruise">
                <option value="${cruise.getId()}">${cruise.getRoute()} by ship ${cruise.getShipId()}</option>
            </c:forEach>
        </select>
        <input type="hidden" name="startdate" value="${startdate}">
        <input type="hidden" name="userId" value="${userId}">
        <input type="hidden" name="Lang" value="${Lang}">
        <br/><br/>
        <input type="submit" value="Submit"/>
    </form>
</div>
</body>
</html>