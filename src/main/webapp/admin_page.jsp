<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Cruise Company</title>
</head>
<body>

<div align="center">
    <h2>Select a Route:</h2>

    <form action="http://localhost:9999/admin/order/status" method="post">
        <table border="1" cellpadding="5">
            <%--<select name="cruiseRoute">--%>
            <tr id="cruiseRoute">
                <th>ID</th>
                <th>Route</th>
                <th>Start Date</th>
                <th>Ship</th>
            </tr>
            <c:forEach var="cruise" items="${cruiseListPag}">
                <tr>
                    <td><c:out value="${cruise.getId()}"/></td>
                    <td><c:out value="${cruise.getRoute()}"/></td>
                    <td><c:out value="${cruise.getStartDate()}"/></td>
                    <td><c:out value="${cruise.getShipId()}"/></td>
                    <td><img src="<c:url value="${cruise.getPicture()}"/>"></td>
                    <td>
                        <input type="hidden" name="Lang" value="${Lang}">
                        <input type="submit" name="cruiseRoute" value="${cruise.getId()}"/>
                            <%--<input type="submit" value="select cruise route"/>--%>
                    </td>
                </tr>
            </c:forEach>
            <%--</select>--%>
        </table>
    </form>

    <form action="http://localhost:9999/admin/order/status" method="post">
        <p id="result"></p>
        <select name="cruiseRoute">
            <c:forEach items="${cruiseListPag}" var="cruise">
                <option value="${cruise.getId()}">${cruise.getRoute()} on ${cruise.getStartDate()}</option>
            </c:forEach>
        </select>
        <br/><br/>
        <input type="hidden" name="Lang" value="${Lang}">
        <c:if test="${currentPage != 1}">
            <td><a href="http://localhost:9999/admin/cruiselistpaginat?page=${currentPage - 1}&Lang=${Lang}"
                   style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Previous</a>
            </td>
        </c:if>
        <c:if test="${currentPage lt noOfPages}">
            <td>
                <a href="http://localhost:9999/admin/cruiselistpaginat?page=${currentPage + 1}&Lang=${Lang}"
                   style="font-size: 15px; color: blue; font-family: arial, helvetica, sans-serif">Next</a>
            </td>
        </c:if>
        <input type="submit" value="select cruise route"/>
    </form>

    <br/><br/>
    <div align="center">
        <form action="http://localhost:9999/admin/newcruise" method="post">
            <input type="hidden" name="Lang" value="${Lang}">
            <input type="submit" value="Add new cruise"/>
        </form>
        <form action="http://localhost:9999/admin/newship" method="post">
            <input type="hidden" name="Lang" value="${Lang}">
            <input type="submit" value="Add new ship"/>
        </form>
    </div>
</div>
</body>
</html>