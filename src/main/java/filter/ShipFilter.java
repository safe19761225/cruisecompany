package filter;

import dao.impl.ShipDAOImpl;
import domain.Ship;
import domain.builder.ShipBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import service.ShipService;
import service.impl.ShipServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static util.Constants.FILTER_SHIP;

/**
 * The {@code ShipFilter}class filter, that is responsible for building new ship from request
 */
@WebFilter(urlPatterns = FILTER_SHIP)
public class ShipFilter implements Filter {

    private final ShipService SERVICE;
    private static final Logger LOGGER = LogManager.getLogger(ShipFilter.class);

    public ShipFilter() {
        super();
        SERVICE = new ShipServiceImpl(new ShipDAOImpl());
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain) throws IOException {

        request.setCharacterEncoding("UTF-8");
        final HttpServletResponse res = (HttpServletResponse) response;
        String capacity = request.getParameter("capacity");
        String numOfVisitedPorts = request.getParameter("num visited ports");
        String staffs = request.getParameter("staffs");
        String lang = request.getParameter("Lang");

        try {
            Integer.parseInt(capacity);
            Integer.parseInt(numOfVisitedPorts);
        } catch (Exception e) {
            if (lang.equals("Ukr")) {
                String s1 = "Будь-ласка введіть правильні кількість пасажирів та відвіданих портів";
                byte[] barr = s1.getBytes();
                ServletOutputStream out = response.getOutputStream();
                out.write(barr);
            } else {
                response.getWriter().println("Please enter correct ship capacity and number of visited ports");
            }
        }
        Ship ship = new ShipBuilder()
                .buildCapacity(Integer.parseInt(capacity))
                .buildNumberPortsVisited(Integer.parseInt(numOfVisitedPorts))
                .buildStuffs(staffs)
                .build();

        int newShipId;
        try {
            newShipId = SERVICE.addNewShip(ship);
        } catch (SQLException throwables) {
            LOGGER.error("Ship can't be added");
            res.setStatus(406);
            return;
        }
        res.setStatus(200);
        res.sendRedirect("/filter/ship/out?newShipId=" + newShipId);
    }
}
