package filter;

import dao.impl.CruiseDAOImpl;
import dao.impl.ShipDAOImpl;
import dao.impl.UserDAOImpl;
import domain.Cruise;

import domain.Ship;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import service.ShipService;
import service.UserService;
import service.impl.CruiseServiceImpl;
import service.impl.ShipServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code AuthenticationFilter} class is front controller filter,
 * that is responsible for processing requests
 */
@WebFilter(urlPatterns = "/auth")
public class AuthenticationFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationFilter.class);
    private final ShipService SHIP_SERVICE;
    private UserService USER_SERVICE;

    public AuthenticationFilter() {
        USER_SERVICE = new UserServiceImpl(new UserDAOImpl());
        SHIP_SERVICE = new ShipServiceImpl(new ShipDAOImpl());
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;
        request.setCharacterEncoding("UTF-8");

        final String login = req.getParameter("login");
        final String password = req.getParameter("password");
        final String lang = req.getParameter("Lang");

        boolean isInDB = USER_SERVICE.verificateUser(login, password);
        boolean isAdmin = USER_SERVICE.isAdmin(login, password);
        int idByLogPas = USER_SERVICE.idByLogPas(login, password);

        CruiseServiceImpl cruiseDao = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Cruise> cruiseList = cruiseDao.getAllCruises();
        List<Cruise> cruiseListForUser = new ArrayList<>();
        for (Cruise c : cruiseList) {
            if (c.getStartDate().isAfter(LocalDate.now()))
                cruiseListForUser.add(c);
        }
        request.setAttribute("cruiseListForUser", cruiseListForUser);
        request.setAttribute("cruiseList", cruiseList);
        List<Ship> shipList = null;
        try {
            shipList = SHIP_SERVICE.getAllShips();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        req.getSession().setAttribute("ships", shipList);

        if (lang.equals("Eng")) {
            if (login.length() >= 5 && password.length() >= 5) {
                if (isInDB) {
                    if (isAdmin) {
                        LOGGER.info("Hello, Admin");
                        request.setAttribute("Lang", lang);
                        req.getServletContext().getRequestDispatcher("/admin/cruiselistpaginat?Lang=" + lang).forward(request, response);
                    } else {
                        LOGGER.info("Hello, User");
                        request.setAttribute("userId", idByLogPas);
                        request.setAttribute("Lang", lang);
                        req.getServletContext().getRequestDispatcher("/user/cruiselistpaginat?Lang=" + lang).forward(request, response);
                    }
                } else {
                    LOGGER.info("Hello, Guest");
                    request.setAttribute("Lang", lang);
                    req.getServletContext().getRequestDispatcher("/regist.jsp").forward(request, response);
                }
            } else {
                LOGGER.info("Login or password is too short by entering");
                boolean lengthMatch = false;
                res.sendRedirect("/filter/index/out?lengthMatch=" + lengthMatch + "&Lang=" + lang);
            }

        } else {
            if (login.length() >= 5 && password.length() >= 5) {
                if (isInDB) {
                    if (isAdmin) {
                        LOGGER.info("Привіт, адмін");
                        request.setAttribute("allCruises", cruiseList);
                        request.setAttribute("Lang", lang);
                        req.getServletContext().getRequestDispatcher("/admin/cruiselistpaginat?Lang=" + lang).forward(request, response);
                    } else {
                        LOGGER.info("Привіт, користувач");
                        request.setAttribute("Lang", lang);
                        request.setAttribute("userId", idByLogPas);
                        req.getServletContext().getRequestDispatcher("/user/cruiselistpaginat?Lang=" + lang).forward(request, response);
                    }
                } else {
                    LOGGER.info("Привіт, гість");
                    request.setAttribute("Lang", lang);
                    req.getServletContext().getRequestDispatcher("/regist_ukr.jsp").forward(request, response);
                }
            } else {
                LOGGER.info("Логін або пароль на вході занадто короткі");
                boolean lengthMatch = false;
                res.sendRedirect("/filter/index/out?lengthMatch=" + lengthMatch + "&Lang=" + lang);
            }
        }
    }
}
