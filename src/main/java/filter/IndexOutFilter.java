package filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static util.Constants.FILTER_INDEX_OUT;

/**
 * The {@code IndexOutFilter} class filter, that is responsible for printing registration warnings from request
 */
@WebFilter(urlPatterns = FILTER_INDEX_OUT)
public class IndexOutFilter implements Filter {

    public IndexOutFilter() {
        super();
    }

    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain filterChain) throws IOException {

        request.setCharacterEncoding("UTF-8");
        final HttpServletRequest req = (HttpServletRequest) request;
        String lengthMatch = req.getParameter("lengthMatch");
        String lang = req.getParameter("Lang");

        if (lengthMatch.equals("false")) {
            if (lang.equals("Ukr")) {
                String s1 = "Ваш логін та пароль повинні мати мінімум 5 знаків." +
                        " Поверніться та спробуйте знову";
                byte[] barr = s1.getBytes();
                ServletOutputStream out = response.getOutputStream();
                out.write(barr);
            }
            response.getWriter().println("Your login and password should be min 5 characters long." +
                    " Roll back and try again");
        }
    }
}
