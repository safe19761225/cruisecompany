package controller;

import dao.impl.CruiseDAOImpl;
import domain.Cruise;
import service.CruiseService;
import service.impl.CruiseServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

import static util.Constants.*;


@WebServlet(urlPatterns = CRUISE_LIST_PAGINAT_SERVLET_FOR_USER)
public class CruiseListForUserPaginServlet extends HttpServlet {

    int userId;
    String sessionId;

    public CruiseListForUserPaginServlet() {
        super();
    }

    /**
     * Processes get-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {


        String lang = request.getParameter("Lang");
        request.setAttribute("Lang", lang);
        request.setAttribute("userId", this.userId);

        int page = 1;
        int recordsPerPage = ROWS_PER_PAGE_FOR_USER;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(
                    request.getParameter("page"));
        CruiseService cruiseService = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Cruise> list = cruiseService.getCruiseListForUserPag(
                (page - 1) * recordsPerPage,
                recordsPerPage);
        int noOfRecords = cruiseService.getNoOfRecordsForUser();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0
                / recordsPerPage);
        request.setAttribute("cruiseListPag", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        if (sessionId.equals(request.getSession().getId())) {
            if (lang.equals("Ukr")) {
                // response.sendRedirect("/admin_page_ukr.jsp");
                RequestDispatcher view
                        = request.getRequestDispatcher("/user_page_ukr.jsp");
                view.forward(request, response);
            } else {
           /* response.sendRedirect("/admin_page.jsp?cruiseListPag="+list+
                                        "&noOfPages="+noOfPages+"&currentPage="+page+"&Lang="+lang);*/
                RequestDispatcher view
                        = request.getRequestDispatcher("/user_page.jsp");
                view.forward(request, response);
            }
        }
    }

    /**
     * Processes post-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, IOException {


        String lang = request.getParameter("Lang");
        request.setAttribute("Lang", lang);
        int userId1 = (Integer) request.getAttribute("userId");
        this.userId = userId1;
        request.setAttribute("userId", this.userId);

        int page = 1;
        int recordsPerPage = ROWS_PER_PAGE_FOR_USER;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(
                    request.getParameter("page"));
        CruiseService cruiseService = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Cruise> list = cruiseService.getCruiseListForUserPag(
                (page - 1) * recordsPerPage,
                recordsPerPage);
        int noOfRecords = cruiseService.getNoOfRecordsForUser();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0
                / recordsPerPage);
        request.setAttribute("cruiseListPag", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        sessionId = request.getSession().getId();

        if (lang.equals("Ukr")) {
            // response.sendRedirect("/admin_page_ukr.jsp");
            RequestDispatcher view
                    = request.getRequestDispatcher("/user_page_ukr.jsp");
            view.forward(request, response);
        } else {
            //  response.sendRedirect("/admin_page.jsp");
            RequestDispatcher view
                    = request.getRequestDispatcher("/user_page.jsp");
            view.forward(request, response);
        }
    }
}
