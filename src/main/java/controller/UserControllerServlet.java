package controller;

import dao.impl.CruiseDAOImpl;
import dao.impl.ShipDAOImpl;
import domain.Cruise;
import service.CruiseService;
import service.impl.CruiseServiceImpl;
import service.impl.ShipServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static util.Constants.USER_CONTROLLER_SERVLET;

/**
 * The {@code UserControllerServlet} class servlet,
 * that is responsible for processing user's requests before making order
 */
@WebServlet(urlPatterns = USER_CONTROLLER_SERVLET)
public class UserControllerServlet extends HttpServlet {

    /**
     * Processes get-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getServletContext().getRequestDispatcher("/user_page.jsp").forward(request, response);
    }

    /**
     * Processes post-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int userId = getUserIdFromRequest(request);
        LocalDate locDat = getLocalDateFromRequest(request);
        int days = getPeriodFromRequest(request);
        String lang = request.getParameter("Lang");
        CruiseService cruiseService = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Integer> shipIds = null;
        try {
            shipIds = cruiseService.getShipIdListByDateDuration(locDat, days);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        request.setAttribute("duration", days);
        request.setAttribute("shipIds", shipIds);
        request.setAttribute("startdate", locDat);
        request.setAttribute("userId", userId);
        request.setAttribute("Lang", lang);
        List<Integer> cruiseIds = null;
        try {
            cruiseIds = cruiseService.getCruiseIdListByStartDateDuration((LocalDate) request.getAttribute("startdate"),
                    (Integer) request.getAttribute("duration"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        List<Cruise> cruiseList = new ArrayList<>();
        for (Integer integer : cruiseIds) {
            try {
                cruiseList.add(cruiseService.getCruiseById(integer));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        request.setAttribute("cruiseList", cruiseList);

        if (lang.equals("Ukr")) {
            request.getServletContext().getRequestDispatcher("/userorder_page_ukr.jsp").forward(request, response);
        } else {
            request.getServletContext().getRequestDispatcher("/userorder_page.jsp").forward(request, response);
        }
    }

    /**
     * Receives request gets  LocalDate from it.
     *
     * @param request {@code HttpServletRequest} to the servlet
     * @return {@code LocalDate}  from request.
     */
    private LocalDate getLocalDateFromRequest(HttpServletRequest request) {

        String localDateString = request.getParameter("startdate");
        return LocalDate.parse(localDateString);
    }

    /**
     * Receives request gets  Period from it.
     *
     * @param request {@code HttpServletRequest} to the servlet
     * @return {@code int} days of Period from request.
     */
    private int getPeriodFromRequest(HttpServletRequest request) {

        String period = request.getParameter("cruiseDuration");
        String days = period.split(" ")[0];
        return Integer.parseInt(days);
    }

    /**
     * Receives request gets  user's Id from it.
     *
     * @param request {@code HttpServletRequest} to the servlet
     * @return {@code int} with user's Id from request.
     */
    private int getUserIdFromRequest(HttpServletRequest request) {

        String userId = request.getParameter("userId");
        return Integer.parseInt(userId);
    }
}


