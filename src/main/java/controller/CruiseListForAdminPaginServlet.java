package controller;

import dao.impl.CruiseDAOImpl;
import domain.Cruise;
import service.CruiseService;
import service.impl.CruiseServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

import static util.Constants.CRUISE_LIST_PAGINAT_SERVLET;
import static util.Constants.ROWS_PER_PAGE;

/**
 * The {@code CruiseListForAdminPaginServlet} class servlet,
 * that is responsible for processing admin's requests to create page and select Cruise
 */
@WebServlet(urlPatterns = CRUISE_LIST_PAGINAT_SERVLET)
public class CruiseListForAdminPaginServlet extends HttpServlet {

    String sessionId;

    public CruiseListForAdminPaginServlet() {
        super();
    }

    /**
     * Processes get-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        String lang = request.getParameter("Lang");
        request.setAttribute("Lang", lang);

        int page = 1;
        int recordsPerPage = ROWS_PER_PAGE;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(
                    request.getParameter("page"));
        CruiseService cruiseService = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Cruise> list = cruiseService.getCruiseListPag(
                (page - 1) * recordsPerPage,
                recordsPerPage);
        int noOfRecords = cruiseService.getNoOfRecords();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0
                / recordsPerPage);
        request.setAttribute("cruiseListPag", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);

        if (sessionId.equals(request.getSession().getId())) {
            if (lang.equals("Ukr")) {
                // response.sendRedirect("/admin_page_ukr.jsp");
                RequestDispatcher view
                        = request.getRequestDispatcher("/admin_page_ukr.jsp");
                view.forward(request, response);
            } else {
           /* response.sendRedirect("/admin_page.jsp?cruiseListPag="+list+
                                        "&noOfPages="+noOfPages+"&currentPage="+page+"&Lang="+lang);*/
                RequestDispatcher view
                        = request.getRequestDispatcher("/admin_page.jsp");
                view.forward(request, response);
            }
        }
    }

    /**
     * Processes post-request
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException when process servlet fails
     * @throws IOException      when process servlet fails
     */
    @Override
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response)
            throws ServletException, IOException {
        String lang = request.getParameter("Lang");
       // String ships = request.getParameter("ships");
        request.setAttribute("Lang", lang);
       /* request.setAttribute("ships");*/

        int page = 1;
        int recordsPerPage = ROWS_PER_PAGE;
        if (request.getParameter("page") != null)
            page = Integer.parseInt(
                    request.getParameter("page"));
        CruiseService cruiseService = new CruiseServiceImpl(new CruiseDAOImpl());
        List<Cruise> list = cruiseService.getCruiseListPag(
                (page - 1) * recordsPerPage,
                recordsPerPage);
        int noOfRecords = cruiseService.getNoOfRecords();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0
                / recordsPerPage);
        request.setAttribute("cruiseListPag", list);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("currentPage", page);
        sessionId = request.getSession().getId();

        if (lang.equals("Ukr")) {
            // response.sendRedirect("/admin_page_ukr.jsp");
            RequestDispatcher view
                    = request.getRequestDispatcher("/admin_page_ukr.jsp");
            view.forward(request, response);
        } else {
            //  response.sendRedirect("/admin_page.jsp");
            RequestDispatcher view
                    = request.getRequestDispatcher("/admin_page.jsp");
            view.forward(request, response);
        }
    }
}
